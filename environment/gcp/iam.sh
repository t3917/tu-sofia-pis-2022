#!/bin/bash

set -e
export PROJECT_ID=$(gcloud config get-value core/project)
export SERVICE_ACCOUNT_NAME="pis-2022";
export SERVICE_ACCOUNT_EMAIL="${SERVICE_ACCOUNT_NAME}@${PROJECT_ID}.iam.gserviceaccount.com"

gcloud iam service-accounts create ${SERVICE_ACCOUNT_NAME} --display-name "Kubernetes Cluster Account" || true;

export ROLES="
roles/bigquery.dataEditor
roles/compute.instanceAdmin.v1
roles/errorreporting.writer
roles/logging.logWriter
roles/monitoring.metricWriter
roles/pubsub.subscriber
roles/storage.objectCreator
roles/stackdriver.resourceMetadata.writer
roles/clouddebugger.agent
";

for role in $ROLES; do
    gcloud projects add-iam-policy-binding ${PROJECT_ID} \
       --member="serviceAccount:${SERVICE_ACCOUNT_EMAIL}" \
       --role=$role || echo "Failed to give service account permissions (needs owner permissions), do this manually";
done;

kubectl create clusterrolebinding add-on-cluster-admin \
	--clusterrole=cluster-admin \
	--serviceaccount=kube-system:default || echo "add-on-cluster-admin already added"
